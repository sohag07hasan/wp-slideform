<style>
	div.slider-popup{
		position: fixed;
		bottom: 0%;
		left: 5%;
		z-index: 10;
	}
	
	div.slider-content{
		background: #6b6a63;
	    margin: 0 auto;
	    padding: 6px;
	    border-radius: .5em 0em 0em 0em;   
	}
	
	div.slider-controller{
		float: right;
		background: #6b6a63;
		border-radius: .2em .2em 0em 0em;
		padding: .2em .3em .2em .3em;	    		
	}
	
	div.clear{
		clear: both;
	}
	
</style>

<div class="slider-popup">
	<div class="slider-controller">
		<img src="<?php echo $this->get_image_src('plus.png'); ?>" >
	</div>
	<div class="clear"></div>
	<div class="slider-content">
		<p class="slider-description"> This is a slider </p>
		<div class="slider-components">
			<div>this is first questions</div>
			<div>This is second questions</div>
		</div>
	</div>
</div>
