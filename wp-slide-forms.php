<?php 

/*
 * Plugin Name: WP SlideForm
 * Author: WP SlideForm
 * Description: Get all the power of Qualaroo insights at a fraction of the price 
 * */

class WpSlideForms{
	
	public $plugin_directory;
	public $plugin_url;
	public $db;
	
	//constructor, holds many important information and kick the init function
	function __construct(){
		$this->plugin_directory = dirname(__FILE__) . '/';
		$this->plugin_url = plugins_url('/', __FILE__);

		return $this->init();
	}
	
	
	/**
	 * includes database class and make an instance of it
	 * */
	function get_db(){
		if($this->db instanceof WpSlideFromsDb) return $this->db;
		
		if(!class_exists('WpSlideFromsDb')){
			include $this->plugin_directory . 'classes/db.php';
		}
		
		$this->db = new WpSlideFromsDb();
		return $this->db;
	}
	
	
	/**
	 * contains basic hooks of the plugin
	 * */
	function init(){
		
		//database tables
		register_activation_hook(__FILE__, array($this->get_db(), 'sync_db'));
		
		//include the survey popup inside footer
		add_action('wp_footer', array(&$this, 'include_survey_popup'));
		
	}
	
	
	/**
	 * Survey popup form to get feedback from visitors	 * 
	 * */
	function include_survey_popup(){
		include $this->plugin_directory . 'includes/survey-poup.html.php';
	}
	
	
	/**
	 * Get image
	 * @file_name = image name
	 * */
	function get_image_src($file_name){
		return $this->plugin_url . 'images/' . $file_name;
	}
	
}


return new WpSlideForms();

?>